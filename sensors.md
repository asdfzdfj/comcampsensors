# Ultrasonic Sensor and IR Photosensor

## Ultra sensor HC-SR04

![HC-SR04](ultrasonic-sensor-HCSR04-1.jpg)

HC-SR04 เป็นเซ็นเซอร์ ultrasonic ที่สามารถใช้ในการวัดระยะทางได้โดยอาศัยหลักการการสะท้อนของเสียง
ultrasonic โดยที่ตัว sensor จะทำการส่งเสียง ultrasonic ความถี่สูงออกไป เมื่อเสียงตกกระทบกับวัตถุ
จะเกิดการสะท้อนกลับและเคลื่อนที่จนกระทั่งกระทบกับตัวตรวจรับที่อยู่บน sensor และตัว sensor
จะให้สัญญาณออกมา

โดยสำหรับ HC-SR04 นี้จะมีวิธีการใช้งานโดยทำการป้อน logic High ให้กับขา Trigger อย่างน้อย
$10\ \mu{}s$ และรอรับคลื่นที่สะท้อนกลับมาจากวัตถุโดยที่ตัว sensor จะให้สัญญาณ pulse logic High
ออกมาจากขา Echo ซึ่งเราสามารถทำการวัดเวลาจากตอนที่เริ่มส่งสัญญาณจนการทั่งถึงตอนที่ได้รับสัญญาณได้
และจากข้อมูลนี้เราสามารถใช้หาระยะห่างระหว่าง sensor กับวัตถุ $s$ ได้ดังสมการ
$$s = v_s \cdot \frac{t}2$$
โดยให้ความเร็วของเสียงในอากาศ $v_s = 340\ m/s$ หรือ $v_s = 0.034\ cm/\mu{s}$
โดยประมาณ และสามารถวัดระยะเวลาจนได้รับสัญญาณ $t$ ที่ผ่านไปได้โดยใช้ function
`pulseIn(pin, state)` โดยจะมีค่าเป็นหน่วย $\mu{s}$ (microsecond)

สังเกตว่าในสมการมีการหารระยะเวลาลงครึ่งหนึ่งเนื่องจากเวลา $t$ ที่วัดได้นั้นเป็นทั้งของขาไปและขากลับ
ดังนั้นระยะทางที่แท้จริงนั้นหาจากเวลาที่ใช้ในการเดินทางจาก sensor ไปถึงวัตถุก็เพียงพอ

### ตัวอย่างการต่อวงจรใช้งาน Sensor

![ตัวอย่างการต่อ sensor](sensor-connections.png)

### ตัวอย่าง code ใช้งาน HC-SR04

```arduino
// pin definitions
const byte trigger_pin = 10;
const byte echo_pin = 9;

// variables
long duration;
int distance;

void setup() {
    pinMode(trigger_pin, OUTPUT);
    pinMode(echo_pin, INPUT);
    Serial.begin(9600);
}

void loop() {
    // reset
    digitalWrite(trigger_pin, LOW);
    delayMicroseconds(2);

    // pulse trigger pin
    digitalWrite(trigger_pin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigger_pin, LOW);

    // measure time until echo pulse
    duration = pulseIn(echo_pin, HIGH);

    // calculate distance in cm
    // 340 m/s = 0.034 cm/us
    distance = duration * 0.034 / 2;

    // print distance, feel free to use this info somewhere else...
    Serial.print("Distance (cm): ");
    Serial.println(distance);
    delay(1000);
}
```

โปรแกรมตัวอย่างนี้จะทำการส่ง pulse ไปยังขา trigger ของ HC-SR04 และทำการวัดระยะเวลาที่ผ่านไป
จนกว่าจะได้สัญญาณจากขา echo ของ HC-SR04 โดยใช้ function `pulseIn()` ในการวัด
หลังจากได้ค่าเวลาแล้ว (หน่วย $\mu{s}$) จะทำการคำนวณระยะทาง (หน่วย $cm$) ตามสมการ
$distance = duration \times 0.034 \div 2$ และพิมพ์ผลลัพธ์ออกทาง Serial ทุกๆ 1 วินาที
ซึ่งสามารถดูได้จาก Serial Monitor (จาก Arduino IDE ไปที่ Tools → Serial Monitor และ
arduino ต้องต่ออยู่กับ computer)

ลองนำวัตถุมาตั้งบังหน้า module ที่ระยะต่างๆ แล้วดูค่าที่ได้ผ่าน Serial Monitor

## IR Reflective Photosensor TCRT5000L

![TCRT5000 Module](tcrt&#32;module-500x500.jpg)

ตัวอย่าง Photosensor Module, ตัว TCRT500 จริงๆแล้วเป็นหลอดดำๆที่อยู่ด้านล่าง

Photosensor TCRT5000L เป็น sensor ที่ใช้สำหรับการวัดความเข้มแสง Infrared (IR) โดยที่ตัว
sensor จะประกอบไปด้วย IR LED ที่จะปล่อยแสง IR ออกมาตลอดเวลา, IR Phototransistor
ที่จะรับแสงที่สะท้อนออกมาจากวัตถุ โดยจะมีแรงดันคร่อม Phototransistor $V_{out}$
เปลี่ยนไปตามแสงที่ได้รับ

โดยทั่วไปแล้ว วัตถุที่นำมาเป็นตัวตกกระทบจะต้องมีคุณสมบัติในการสะท้อนแสงที่ต่างกัน เช่นโต๊ะสีขาว
(สะท้อนแสงได้ดี) กับแถบสีดำ (สีดำสะท้อนแสงได้น้อย)

ในการนำไปใช้งาน อย่างเช่นในการทำหุ่นยนต์เดินตามเส้น มักจะนำ output จาก Phototransistor
ไปต่อกับวงจร comparator เพื่อใช้ในการตรวจจับและตัดสินว่าวัตถุที่อยู่ตรงหน้า sensor ว่าเป็นสีดำหรือสีขาว
ให้มีค่าออกมาในรูปแบบสัญญาณ digital แทนที่จะเป็นสัญญาณ analog โดยสามารถปรับค่าในการตรวจจับได้ด้วยตัวต้านทานปรับค่าได้ที่อยู่ในวงจร comparator

## ตัวอย่าง code ใช้งาน IR Sensor

ใช้วงจรเดียวกันกับของ HC-SR04 ข้างต้น

```arduino
// pin definition
const byte sensor_pin = 8;
const byte led_pin = 13;

void setup() {
    pinMode(sensor_pin, INPUT);
}

void loop() {
    // read sensor states
    bool reading = digitalRead(sensor_pin);

    // set on board LED (pin 13) state by reading value
    // feel free to use reading for something else...
    digitalWrite(led_pin, reading);
}
```

โดยโปรแกรมนี้จะทำการอ่านค่าจาก Photosensor แล้วทำการเปิดปิดหลอด LED ที่อยู่บนบอร์ด arduino (pin 13) ตามค่าที่อ่านได้

ลองนำแถบกระดาษสีขาวหรือสีดำมาตั้งหน้า photosensor ที่ระยะประมาณ 5 mm
แล้วสังเกตการเปลี่ยนแปลงของหลอด LED บนบอร์ด arduino

ลองตั้งแถบสีดำไว้ที่ระยะต่างๆในช่วงประมาณ 0.5 ถึง 1.5 cm จากตัวหลอดบน module แล้วทำการปรับตัวต้านทานปรับค่าได้ที่อยู่หลัง module ด้วยไขควงช้าๆ
สังเกตการเปลี่ยนแปลงของหลอด LED เมื่อเจอจุดที่ทำให้สถานะของค่าที่อ่านได้จาก module เปลี่ยนไป
ให้บิดกลับเล็กน้อยให้ค่าที่อ่านได้เป็นค่าเดิมก่อนการเปลี่ยนแปลง แล้วลองเลื่อนแถบสีดำให้เป็นสีขาว ดูว่าตัว module สามารถตรวจจับความแตกต่างได้หรือไม่